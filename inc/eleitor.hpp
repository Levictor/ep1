#ifndef ELEITOR_HPP
#define ELEITOR_HPP

#include "presidente.hpp"
#include "governador.hpp"
#include "senador.hpp"
#include "deputado_distrital.hpp"
#include "deputado_federal.hpp"
#include <vector>

using namespace std;

class Eleitor{
private:
    string voto_deputado_federal;
    string voto_deputado_distrital;
    string voto_senador_1;
    string voto_senador_2;
    string voto_governador;
    string voto_presidente;
    string nome_eleitor;
    int nr_voto_eleitor;

public:
    
    Eleitor();
    ~Eleitor();
    
    void set_voto_deputado_federal(string voto_deputado_federal);
    string get_voto_deputado_federal();

    void set_voto_deputado_distrital(string voto_deputado_distrital);
    string get_voto_deputado_distrital();

    void set_voto_senador_1(string voto_senador_1);
    string get_voto_senador_1();

    void set_voto_senador_2(string voto_senador_2);
    string get_voto_senador_2();

    void set_voto_governador(string voto_governador);
    string get_voto_governador();

    void set_voto_presidente(string voto_presidente);
    string get_voto_presidente();
    
    void set_nome_eleitor(string nome_eleitor);
    string get_nome_eleitor();
    
    void relatar_votos(Presidente presidentes[],
        Governador governadores[],
        Senador senadores[],
        DeputadoDistrital deputadosDistritais[],
        DeputadoFederal deputadosFederais[],
        vector<Eleitor> eleitores,
        int numero_eleitor);
    int tratar_voto(string voto);
};

#endif
