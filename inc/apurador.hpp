#ifndef APURADOR_HPP
#define APURADOR_HPP
#define CD_PRESIDENTE 0
#define CD_GOVERNADOR 1

#include "urna.hpp"

class Apurador {
public:
    void apurar_votos(Presidente presidentes[],Governador governadores[], Senador senadores[], vector<Eleitor> eleitores, int numero_votos);

private:
    void apurar_presidente(Presidente presidentes[], int numero_votos, vector<Eleitor> eleitores);
    void apurar_governador(Governador governadores[], int numero_votos, vector<Eleitor> eleitores);
    void apurar_senadores(Senador senadores[]);
    int calcular_votos_efetivos(vector<Eleitor> eleitores, int cd_cargo, int nr_votos);
    Presidente presidente_vencedor;
    Governador governador_vencedor;
    Presidente presidente_segundo_lugar;
    Governador governador_segundo_lugar;
    Senador senador_vencedor[2];
    Senador senador_segundo_lugar;

};


#endif
