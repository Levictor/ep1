#ifndef CANDIDATOS_HPP
#define CANDIDATOS_HPP
#define NR_PRESIDENTES 26
#define NR_GOVERNADORES 22
#define NR_SENADORES 60
#define NR_DEPUTADOS_DISTRITAIS 969
#define NR_DEPUTADOS_FEDERAIS 185

#include <fstream>
#include <iostream>
#include <string>

using namespace std;

class Candidatos{
protected:
    bool condicao_voto_nulo = true;
    string NM_UE;
    string DS_CARGO;
    int NR_CANDIDATO;
    string NM_CANDIDATO;
    string NM_URNA_CANDIDATO;
    int NR_PARTIDO;
    string SG_PARTIDO;
    string DS_PARTIDO;
    int NR_IDADE_POSSE;
    int CD_GENERO;

public:
    int numero_votos;

    void set_nm_ue(string nm_ue);
    string get_nm_ue();

    void set_ds_cargo(string ds_cargo);
    string get_ds_cargo();

    void set_nr_candidato(string nr_candidato);
    int get_nr_candidato();

    void set_nm_candidato(string nm_candidato);
    string get_nm_candidato();

    void set_nm_urna_candidato(string nm_urna_candidato);
    string get_nm_urna_candidato();

    void set_nr_partido(string nr_partido);
    int get_nr_partido();

    void set_sg_partido(string sg_partido);
    string get_sg_partido();

    void set_ds_partido(string ds_partido);
    string get_ds_partido();

    void set_nr_idade_posse(string nr_idade_posse);
    int get_nr_idade_posse();

    void set_cd_genero(string cd_genero);
    int get_cd_genero();

    void set_nr_votos(int voto);
    int get_nr_votos();
    
    virtual void mostrar_candidato();
};

#endif
