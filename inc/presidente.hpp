#ifndef PRESIDENTE_HPP
#define PRESIDENTE_HPP

#include "candidatos.hpp"

class Presidente : public Candidatos{

public:
    Presidente();
    void preencher(Presidente presidentes[]);
    void votar(int nr_candidato, Presidente presidentes[]);
    bool mostrar_candidato(int nr_candidato, Presidente presidentes[]);

private:
    string vice_presidente;
};


#endif
