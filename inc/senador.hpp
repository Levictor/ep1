#ifndef SENADOR_HPP
#define SENADOR_HPP

#include "candidatos.hpp"

class Senador : public Candidatos{

public:
    Senador();
    void preencher(Senador senador[]);
    void votar(int nr_voto, Senador senadores[]);
    bool mostrar_candidato(int nr_candidato, Senador senadores[], int primeiro_voto, int vaga);

private:
    string primeiro_suplente;
    string segundo_suplente;
};


#endif
