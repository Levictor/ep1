#ifndef DEPUTADO_FEDERAL_HPP
#define DEPUTADO_FEDERAL_HPP

#include "candidatos.hpp"

class DeputadoFederal : public Candidatos{

public:
    DeputadoFederal();

    void preencher(DeputadoFederal deputadosFederais[]);
    void votar(int nr_voto, DeputadoFederal deputadosFederais[]);
    bool mostrar_candidato(int nr_candidato, DeputadoFederal deputadosFederais[]);
};


#endif
