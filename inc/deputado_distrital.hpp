#ifndef DEPUTADO_DISTRITAL_HPP
#define DEPUTADO_DISTRITAL_HPP

#include "candidatos.hpp"

class DeputadoDistrital : public Candidatos{
public:
    DeputadoDistrital();
    void preencher(DeputadoDistrital deputadoDistrital[]);
    void votar(int nr_candidato, DeputadoDistrital depuadoDistrital[]);
    bool mostrar_candidato(int nr_candidato, DeputadoDistrital depuadoDistrital[]);

};


#endif
