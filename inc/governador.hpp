#ifndef GOVERNADOR_HPP
#define GOVERNADOR_HPP

#include "candidatos.hpp"

class Governador : public Candidatos{
public:
    Governador();
    void preencher(Governador governador[]);
    void votar(int nr_voto, Governador governadores[]);
    bool mostrar_candidato(int nr_candidato, Governador governadores[]);
private:
    string vice_governador;
};


#endif
