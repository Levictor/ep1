#ifndef URNA_HPP
#define URNA_HPP

#include "presidente.hpp"
#include "senador.hpp"
#include "deputado_distrital.hpp"
#include "deputado_federal.hpp"
#include "governador.hpp"
#include "eleitor.hpp"

class Urna {
public:
    vector<Eleitor> iniciar_votacao(Presidente presidentes[],
                        Governador governadores[],
                        Senador senadores[],
                        DeputadoDistrital deputadosDistritais[],
                        DeputadoFederal deputadosFederais[],
                        vector<Eleitor> eleitor,
                        int numero_eleitor);
    int get_votos_brancos();
    int get_votos_nulos();

private:
    int votos_brancos = 0;
    int votos_nulos = 0;
    bool condicao_voto_nulo; //TRUE = voto nulo;
    string nome_eleitor;
    bool condicao_voto; //TRUE = o voto está em andamento
    string decisao_voto;
    string voto;
    int nr_voto;
    int verificar_voto();
    bool confirmar_voto();
};

#endif