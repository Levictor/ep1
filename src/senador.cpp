#include "senador.hpp"

Senador::Senador(){
    
}

void Senador::preencher(Senador senadores[]){
  fstream ip("./data/consulta_cand_2018_DF.csv");


  if(!ip.good())
    cout<<"Não foi possivel abrir o arquivo"<<endl;

  string nm_ue;
  string ds_cargo;
  string nr_candidato;
  string nm_candidato;
  string nm_urna_candidato;
  string nr_partido;
  string sg_partido;
  string ds_partido;
  string nr_idade_posse;
  string cd_genero;
  
  int i=0;
  while(ip.good()){

    getline(ip,nm_ue,',');
    getline(ip,ds_cargo,',');
    getline(ip,nr_candidato,',');
    getline(ip,nm_candidato,',');
    getline(ip,nm_urna_candidato,',');
    getline(ip,nr_partido,',');
    getline(ip,sg_partido,',');
    getline(ip,ds_partido,',');
    getline(ip,cd_genero,',');
    getline(ip,nr_idade_posse);

    if(ds_cargo.compare("SENADOR") == 0 || ds_cargo.compare("PRIMEIRO SUPLENTE") == 0 || ds_cargo.compare("SEGUNDO SUPLENTE") == 0){
      senadores[i].set_nm_ue(nm_ue);
      senadores[i].set_ds_cargo(ds_cargo);
      senadores[i].set_nr_candidato(nr_candidato);
      senadores[i].set_nm_candidato(nm_candidato);
      senadores[i].set_nm_urna_candidato(nm_urna_candidato);
      senadores[i].set_nr_partido(nr_partido);
      senadores[i].set_sg_partido(sg_partido);
      senadores[i].set_ds_partido(ds_partido);
      senadores[i].set_nr_votos(0);
      senadores[i].set_nr_idade_posse(nr_idade_posse);
      senadores[i].set_cd_genero(cd_genero);

      i++;
    }

  }
}

void Senador::votar(int nr_candidato, Senador senadores[]){
  if(nr_candidato == 0)
    return;

  for(int i=0; i < NR_SENADORES; i++){
    int nr_candidato_array = senadores[i].get_nr_candidato();

    if(nr_candidato_array == nr_candidato){
      int votos = senadores[i].get_nr_votos() + 1;
      senadores[i].set_nr_votos(votos);
    }
  }
}

bool Senador::mostrar_candidato(int nr_candidato, Senador senadores[], int primeiro_voto, int vaga){
  condicao_voto_nulo = true;

  if (vaga == 1){
    cout << "VOTO PARA SENADOR PRIMEIRA VAGA" << endl;
  }

  else{
    cout << "VOTO PARA SENADOR SEGUNDA VAGA" << endl;
  }

  if (nr_candidato == 0){
    cout << "VOTO EM BRANCO" << endl << endl;
    return false;
  }
  if(primeiro_voto == nr_candidato){
    cout << "VOTO NULO, MESMO VOTO DO PRIMEIRO SENADOR" << endl << endl;
    return true;
  }

  for(int i=0; i < NR_SENADORES; i++){
    int nr_candidato_array = senadores[i].get_nr_candidato();
    int cd_genero_array = senadores[i].get_cd_genero();
    string cargo_candidato_array = senadores[i].get_ds_cargo();

    if(nr_candidato_array == nr_candidato && cargo_candidato_array.compare("SENADOR") == 0){
      cout << "Nome: " << senadores[i].get_nm_urna_candidato() << endl;
      cout << "Nome completo: " << senadores[i].get_nm_candidato() << endl;
      cout << "Partido: " << senadores[i].get_sg_partido() << endl;
      if (cd_genero_array == 2){
        cout << "Número do candidato: " << senadores[i].get_nr_candidato() << endl;
      }
      else {
        cout << "Número da candidata: " << senadores[i].get_nr_candidato() << endl;
      }
      condicao_voto_nulo = false;
    }

    if(cargo_candidato_array.compare("PRIMEIRO SUPLENTE") == 0 && nr_candidato_array == nr_candidato){
      primeiro_suplente = senadores[i].get_nm_urna_candidato();
    }
    if(cargo_candidato_array.compare("SEGUNDO SUPLENTE") == 0 && nr_candidato_array == nr_candidato){
      segundo_suplente = senadores[i].get_nm_urna_candidato();
    }
  }

  if(condicao_voto_nulo)
    cout << "VOTO NULO" << endl << endl;


  if(!condicao_voto_nulo){
    cout << "PRIMEIRO SUPLENTE: " << primeiro_suplente << endl;
    cout << "SEGUNDO SUPLENTE: " << segundo_suplente << endl << endl;
  }
  return condicao_voto_nulo;
}

