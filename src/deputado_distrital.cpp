#include "deputado_distrital.hpp"

DeputadoDistrital::DeputadoDistrital(){

}

void DeputadoDistrital::preencher(DeputadoDistrital deputados_distritais[]){
  fstream ip("./data/consulta_cand_2018_DF.csv");

  if(!ip.good())
    cout<<"Não foi possivel abrir o arquivo"<<endl;

  string nm_ue;
  string ds_cargo;
  string nr_candidato;
  string nm_candidato;
  string nm_urna_candidato;
  string nr_partido;
  string sg_partido;
  string ds_partido;
  string nr_idade_posse;
  string cd_genero;
  
  int i=0;
  while(ip.good()){
    getline(ip,nm_ue,',');
    getline(ip,ds_cargo,',');
    getline(ip,nr_candidato,',');
    getline(ip,nm_candidato,',');
    getline(ip,nm_urna_candidato,',');
    getline(ip,nr_partido,',');
    getline(ip,sg_partido,',');
    getline(ip,ds_partido,',');
    getline(ip,cd_genero,',');
    getline(ip,nr_idade_posse);

    if(ds_cargo.compare("DEPUTADO DISTRITAL") == 0){
      if(stoi(nr_candidato,nullptr,10) == 70099){
        nm_urna_candidato.replace(13, 1, 1,',');
      }
      deputados_distritais[i].set_nr_votos(0);
      deputados_distritais[i].set_nm_ue(nm_ue);
      deputados_distritais[i].set_ds_cargo(ds_cargo);
      deputados_distritais[i].set_nr_candidato(nr_candidato);
      deputados_distritais[i].set_nm_candidato(nm_candidato);
      deputados_distritais[i].set_nm_urna_candidato(nm_urna_candidato);
      deputados_distritais[i].set_nr_partido(nr_partido);
      deputados_distritais[i].set_sg_partido(sg_partido);
      deputados_distritais[i].set_ds_partido(ds_partido);
      deputados_distritais[i].set_nr_idade_posse(nr_idade_posse);
      deputados_distritais[i].set_cd_genero(cd_genero);

      i++;
    }

  }
}

void DeputadoDistrital::votar(int nr_candidato, DeputadoDistrital deputados_distritais[]){
  for(int i=0; i < NR_DEPUTADOS_DISTRITAIS; i++){
    int nr_candidato_array = deputados_distritais[i].get_nr_candidato();
    int nr_partido_array = deputados_distritais[i].get_nr_partido();
    
    if(nr_candidato_array == nr_candidato){
      int votos = deputados_distritais[i].get_nr_votos() + 1;
      deputados_distritais[i].set_nr_votos(votos);
    }
    if(nr_partido_array != 0){
      if(nr_partido_array == nr_candidato){
        int votos = deputados_distritais[i].get_nr_votos() + 1;
        deputados_distritais[i].set_nr_votos(votos);
      }
    }
  }
}

bool DeputadoDistrital::mostrar_candidato(int nr_candidato, DeputadoDistrital deputados_distritais[]){
  condicao_voto_nulo = true;
  cout << "VOTO PARA DEPUTADO DISTRITAL" << endl;
  bool voto_na_legendas = false;
  string partido;
  
  if (nr_candidato == 0){
    cout << "VOTO EM BRANCO" << endl << endl;
    return false;
  }

  for(int i=0; i < NR_DEPUTADOS_DISTRITAIS; i++){
    int nr_candidato_array = deputados_distritais[i].get_nr_candidato();
    int nr_partido_array = deputados_distritais[i].get_nr_partido();
    int cd_genero_array = deputados_distritais[i].get_cd_genero();

    if(nr_candidato_array == nr_candidato){
      cout << "Nome: " << deputados_distritais[i].get_nm_urna_candidato() << endl;
      cout << "Nome completo: " << deputados_distritais[i].get_nm_candidato() << endl;
      cout << "Partido: " << deputados_distritais[i].get_sg_partido() << endl;
      if (cd_genero_array == 2){
        cout << "Número do candidato: " << deputados_distritais[i].get_nr_candidato() << endl << endl;
      }
      else {
        cout << "Número da candidata: " << deputados_distritais[i].get_nr_candidato() << endl << endl;
      }
      condicao_voto_nulo = false;
    }
    if(nr_partido_array != 0){
      if(nr_partido_array == nr_candidato){
        condicao_voto_nulo = false;
        voto_na_legendas = true;
        partido = deputados_distritais[i].get_ds_partido();
        break;
      }
    }
  }

  if(condicao_voto_nulo){
    cout << "VOTO NULO" << endl << endl;
  }

  if (voto_na_legendas){
    cout << "VOTO NA LEGENDA DO PARTIDO: " << partido << endl << endl;
  }

  return condicao_voto_nulo;
}
