#include "apurador.hpp"

void Apurador::apurar_votos(Presidente presidentes[],Governador governadores[], Senador senadores[], vector<Eleitor> eleitores, int numero_votos){
  this->apurar_senadores(senadores);
  this->apurar_governador(governadores, numero_votos, eleitores);
  this->apurar_presidente(presidentes, numero_votos, eleitores);
}

void Apurador::apurar_senadores(Senador senadores[]){
  cout << "RESULTADO DA VOTAÇÃO PARA SENADOR" << endl;
  senador_vencedor[0].set_nr_votos(0);
  senador_vencedor[1].set_nr_votos(0);

  for(int i=0; i<NR_SENADORES; i++){
    if(senador_vencedor[0].get_nr_votos() < senadores[i].get_nr_votos() && senadores[i].get_ds_cargo().compare("SENADOR") == 0){
      senador_vencedor[0] = senadores[i];
    }
 }
  
  for(int i=0; i<NR_SENADORES; i++){
    if(senador_vencedor[1].get_nr_votos() < senadores[i].get_nr_votos() && senadores[i].get_ds_cargo().compare("SENADOR") == 0){
      if(senadores[i].get_nr_votos() <= senador_vencedor[0].get_nr_votos() && senador_vencedor[0].get_nr_candidato() != senadores[i].get_nr_candidato()){
        senador_vencedor[1] = senadores[i];
      }
    }
    if(senadores[i].get_nr_votos() == senador_vencedor[1].get_nr_votos() && senadores[i].get_nr_idade_posse() > senador_vencedor[1].get_nr_idade_posse() && senadores[i].get_ds_cargo().compare("SENADOR") == 0){
      senador_vencedor[1] = senadores[i];
    }
  }

  if (senador_vencedor[0].get_nr_votos() > 0){
    cout << "SENADORES VENCEDORES" << endl;
    cout << senador_vencedor[0].get_nm_urna_candidato() << endl;
    cout << senador_vencedor[1].get_nm_urna_candidato() << endl << endl;
  }
  else{
    cout << "NÃO HOUVERAM VOTOS EFETIVOS SUFICIENTES" << endl << endl;
  }
}

void Apurador::apurar_governador(Governador governadores[], int numero_votos, vector<Eleitor> eleitores){
  cout << "RESULTADO DA VOTAÇÃO PARA GOVERNADOR" << endl;
  governador_vencedor.set_nr_votos(0);
  governador_segundo_lugar.set_nr_votos(0);
  int votos_efetivos = this->calcular_votos_efetivos(eleitores,CD_GOVERNADOR,numero_votos);

  for(int i=0; i<NR_GOVERNADORES; i++){
    if(governador_vencedor.get_nr_votos() < governadores[i].get_nr_votos() && governadores[i].get_ds_cargo().compare("GOVERNADOR") == 0){
      governador_vencedor = governadores[i];
    }
  }

  for(int i=0; i<NR_GOVERNADORES; i++){      
    if(governador_segundo_lugar.get_nr_votos() < governadores[i].get_nr_votos() && governadores[i].get_ds_cargo().compare("GOVERNADOR") == 0){
      if(governadores[i].get_nr_votos() <= governador_vencedor.get_nr_votos() && governador_vencedor.get_nr_candidato() != governadores[i].get_nr_candidato()){
        governador_segundo_lugar = governadores[i];
      }
    }
    if(governador_segundo_lugar.get_nr_votos() == governadores[i].get_nr_votos() && governadores[i].get_nr_idade_posse() > governador_segundo_lugar.get_nr_idade_posse() && governadores[i].get_ds_cargo().compare("GOVERNADOR") == 0){
      governador_segundo_lugar = governadores[i];
    }
  }
  if(governador_vencedor.get_nr_votos() > votos_efetivos/2){
    if(governador_vencedor.get_cd_genero() == 2){
      cout << "GOVERNADOR VENCEDOR" << endl;
    }
    else {
      cout << "GOVERNADORA VENCEDORA" << endl;
    }
    cout << governador_vencedor.get_nm_urna_candidato() << endl << endl;
  }
  else if(governador_vencedor.get_nr_votos() == 0){
    cout << "NÃO HOUVERAM VOTOS EFETIVOS SUFICIENTES" << endl << endl;
  }
  else{
    cout << "HAVERÁ UM SEGUNDO TURNO ENTRE: " << governador_vencedor.get_nm_urna_candidato() << " E " << governador_segundo_lugar.get_nm_urna_candidato() << endl << endl;
  }
}

void Apurador::apurar_presidente(Presidente presidentes[], int numero_votos, vector<Eleitor> eleitores){
  cout << "RESULTADO DA VOTAÇÃO PARA PRESIDENTE" << endl;
  presidente_vencedor.set_nr_votos(0);
  presidente_segundo_lugar.set_nr_votos(0);
  int votos_efetivos = this->calcular_votos_efetivos(eleitores, CD_PRESIDENTE, numero_votos);

  for(int i=0; i<NR_PRESIDENTES; i++){      
    if(presidente_vencedor.get_nr_votos() < presidentes[i].get_nr_votos() && presidentes[i].get_ds_cargo().compare("PRESIDENTE") == 0){
      presidente_vencedor = presidentes[i];
    }
  }
  for(int i=0; i<NR_PRESIDENTES; i++){      
    if(presidente_segundo_lugar.get_nr_votos() < presidentes[i].get_nr_votos() && presidentes[i].get_ds_cargo().compare("PRESIDENTE") == 0){
      if(presidentes[i].get_nr_votos() <= presidente_vencedor.get_nr_votos() && presidente_vencedor.get_nr_candidato() != presidentes[i].get_nr_candidato()){
        presidente_segundo_lugar = presidentes[i];
      }
    }
    if(presidente_segundo_lugar.get_nr_votos() == presidentes[i].get_nr_votos() && presidentes[i].get_nr_idade_posse() > presidente_segundo_lugar.get_nr_idade_posse() && presidentes[i].get_ds_cargo().compare("PRESIDENTE") == 0){
      presidente_segundo_lugar = presidentes[i];
    }
  }
  if(presidente_vencedor.get_nr_votos() > votos_efetivos/2){
    if(presidente_vencedor.get_cd_genero() == 2){
      cout << "PRESIDENTE VENCEDOR" << endl;
    }
    else {
      cout << "PRESIDENTA VENCEDORA" << endl;
    }
    cout << presidente_vencedor.get_nm_urna_candidato() << endl << endl;
  }
  else if(presidente_vencedor.get_nr_votos() == 0){
    cout << "NÃO HOUVERAM VOTOS EFETIVOS SUFICIENTES" << endl << endl;
  }
  else{
    cout << "HAVERÁ UM SEGUNDO TURNO ENTRE: " << presidente_vencedor.get_nm_urna_candidato() << " E " << presidente_segundo_lugar.get_nm_urna_candidato() << endl << endl;
  }
}

int Apurador::calcular_votos_efetivos(vector<Eleitor> eleitores, int cd_cargo,  int nr_votos){
  int votos_efetivos = 0;
  string voto_eleitor;
  switch(cd_cargo){
    case 0:
      for(int i=0; i<nr_votos; i++){
        voto_eleitor = eleitores[i].get_voto_presidente();
        if(voto_eleitor.compare("0") != 0 && voto_eleitor.compare("NULO") != 0)
          votos_efetivos++;
      }
      break;
    case 1:
      for(int i=0; i<nr_votos; i++){
        voto_eleitor = eleitores[i].get_voto_governador();
        if(voto_eleitor.compare("0") != 0 && voto_eleitor.compare("NULO") != 0)
          votos_efetivos++;
      }
      break;
  }
  return votos_efetivos;
}
