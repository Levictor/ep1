#include "eleitor.hpp"
#include "urna.hpp"
#include "apurador.hpp"
#include <vector>

int main(){
    Presidente presidentes[NR_PRESIDENTES];
    presidentes->preencher(presidentes);
    Governador governadores[NR_GOVERNADORES];
    governadores->preencher(governadores);
    Senador senadores[NR_SENADORES];
    senadores->preencher(senadores);
    DeputadoDistrital deputadosDistritais[NR_DEPUTADOS_DISTRITAIS];
    deputadosDistritais->preencher(deputadosDistritais);
    DeputadoFederal deputadosFederais[NR_DEPUTADOS_FEDERAIS];
    deputadosFederais->preencher(deputadosFederais);
    Urna urna;
    int numero_pessoas;
    system("clear");

    cout << "Digite o número de eleitores, por favor" << endl;
    string entrada;
    bool loop_entradas = true;
    while (loop_entradas){
        cin >> entrada;

        try{
            numero_pessoas = stoi(entrada,nullptr,10);

            if (numero_pessoas < 0){
                throw(1.0);
            }
            vector<Eleitor> eleitores(numero_pessoas);
            eleitores = urna.iniciar_votacao(presidentes, governadores, senadores, deputadosDistritais, deputadosFederais, eleitores, numero_pessoas);
            eleitores[0].relatar_votos(presidentes, governadores, senadores, deputadosDistritais, deputadosFederais, eleitores, numero_pessoas);

            cout << "VOTOS EM BRANCO: " << urna.get_votos_brancos() << endl;
            cout << "VOTOS NULOS: " << urna.get_votos_nulos() << endl << endl;

            Apurador ap;
            ap.apurar_votos(presidentes, governadores, senadores, eleitores, numero_pessoas);
            loop_entradas = false;
        }
        catch(double a){
            cout << "DIGITE UM NÚMERO MAIOR QUE 0" << endl;
        }
        catch(exception e){
            cout << "DIGITE UM NÚMERO" << endl;
        }
    }
    return 0;
}
