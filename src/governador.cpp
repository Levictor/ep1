#include "governador.hpp"

Governador::Governador(){
    
}

void Governador::preencher(Governador governadores[]){
  fstream ip("./data/consulta_cand_2018_DF.csv");

  if(!ip.good())
    cout<<"Não foi possivel abrir o arquivo"<<endl;

  string nm_ue;
  string ds_cargo;
  string nr_candidato;
  string nm_candidato;
  string nm_urna_candidato;
  string nr_partido;
  string sg_partido;
  string ds_partido;
  string nr_idade_posse;
  string cd_genero;
  
  int i=0;
  while(ip.good()){
    getline(ip,nm_ue,',');
    getline(ip,ds_cargo,',');
    getline(ip,nr_candidato,',');
    getline(ip,nm_candidato,',');
    getline(ip,nm_urna_candidato,',');
    getline(ip,nr_partido,',');
    getline(ip,sg_partido,',');
    getline(ip,ds_partido,',');
    getline(ip,cd_genero,',');
    getline(ip,nr_idade_posse);

    if(ds_cargo.compare("GOVERNADOR") == 0 || ds_cargo.compare("VICE-GOVERNADOR") == 0){
      governadores[i].set_nr_votos(0);
      governadores[i].set_nm_ue(nm_ue);
      governadores[i].set_ds_cargo(ds_cargo);
      governadores[i].set_nr_candidato(nr_candidato);
      governadores[i].set_nm_candidato(nm_candidato);
      governadores[i].set_nm_urna_candidato(nm_urna_candidato);
      governadores[i].set_nr_partido(nr_partido);
      governadores[i].set_sg_partido(sg_partido);
      governadores[i].set_ds_partido(ds_partido);
      governadores[i].set_nr_idade_posse(nr_idade_posse);
      governadores[i].set_cd_genero(cd_genero);

      i++;
    }

  }
}

void Governador::votar(int nr_candidato, Governador governadores[]){
  for(int i=0; i < NR_GOVERNADORES; i++){
    int nr_candidato_array = governadores[i].get_nr_candidato();

    if(nr_candidato_array == nr_candidato){
      int votos = governadores[i].get_nr_votos() + 1;
      governadores[i].set_nr_votos(votos);
    }
  }
}

bool Governador::mostrar_candidato(int nr_candidato, Governador governadores[]){
  condicao_voto_nulo = true;
  cout << "VOTO PARA GOVERNADOR" << endl;

  if (nr_candidato == 0){
    cout << "VOTO EM BRANCO" << endl << endl;
    return false;
  }

  for(int i=0; i < NR_GOVERNADORES; i++){
    int nr_candidato_array = governadores[i].get_nr_candidato();
    int cd_genero_array = governadores[i].get_cd_genero();
    string cargo_candidato_array = governadores[i].get_ds_cargo();

    if(nr_candidato_array == nr_candidato && cargo_candidato_array.compare("GOVERNADOR") == 0){
      cout << "Nome: " << governadores[i].get_nm_urna_candidato() << endl;
      cout << "Nome completo: " << governadores[i].get_nm_candidato() << endl;
      cout << "Partido: " << governadores[i].get_ds_partido() << endl;
      if (cd_genero_array == 2){
        cout << "Número do candidato: " << governadores[i].get_nr_candidato() << endl;
      }
      else {
        cout << "Número da candidata: " << governadores[i].get_nr_candidato() << endl;
      }
      condicao_voto_nulo = false;
    }
    if(cargo_candidato_array.compare("VICE-GOVERNADOR") == 0 && nr_candidato_array == nr_candidato)
      vice_governador = governadores[i].get_nm_candidato();
  }

  if(condicao_voto_nulo)
    cout << "VOTO NULO" << endl << endl;
  
  if(!condicao_voto_nulo)
    cout << "VICE-GOVERNADOR: " << vice_governador << endl << endl; 
  
  return condicao_voto_nulo;
}
