#include "deputado_federal.hpp"

DeputadoFederal::DeputadoFederal(){
}

void DeputadoFederal::preencher(DeputadoFederal deputados_federais[]){
  fstream ip("./data/consulta_cand_2018_DF.csv");
  if(!ip.good())
    cout<<"Não foi possivel abrir o arquivo"<<endl;

  string nm_ue;
  string ds_cargo;
  string nr_candidato;
  string nm_candidato;
  string nm_urna_candidato;
  string nr_partido;
  string sg_partido;
  string ds_partido;
  string nr_idade_posse;
  string cd_genero;
  
  int i=0;
  while(ip.good()){

    getline(ip,nm_ue,',');
    getline(ip,ds_cargo,',');
    getline(ip,nr_candidato,',');
    getline(ip,nm_candidato,',');
    getline(ip,nm_urna_candidato,',');
    getline(ip,nr_partido,',');
    getline(ip,sg_partido,',');
    getline(ip,ds_partido,',');
    getline(ip,cd_genero,',');
    getline(ip,nr_idade_posse);

    if(ds_cargo.compare("DEPUTADO FEDERAL") == 0){
      deputados_federais[i].set_nr_votos(0);
      deputados_federais[i].set_nm_ue(nm_ue);
      deputados_federais[i].set_ds_cargo(ds_cargo);
      deputados_federais[i].set_nr_candidato(nr_candidato);
      deputados_federais[i].set_nm_candidato(nm_candidato);
      deputados_federais[i].set_nm_urna_candidato(nm_urna_candidato);
      deputados_federais[i].set_nr_partido(nr_partido);
      deputados_federais[i].set_sg_partido(sg_partido);
      deputados_federais[i].set_ds_partido(ds_partido);
      deputados_federais[i].set_nr_idade_posse(nr_idade_posse);
      deputados_federais[i].set_cd_genero(cd_genero);

      i++;

    }

  }
}

void DeputadoFederal::votar(int nr_candidato, DeputadoFederal deputados_federais[]){
  for(int i=0; i < NR_DEPUTADOS_FEDERAIS; i++){
    int nr_candidato_array = deputados_federais[i].get_nr_candidato();
    int nr_partido_array = deputados_federais[i].get_nr_partido();

    if(nr_candidato_array == nr_candidato){
      int votos = deputados_federais[i].get_nr_votos() + 1;
      deputados_federais[i].set_nr_votos(votos);
    }

    if(nr_partido_array == nr_candidato){
      int votos = deputados_federais[i].get_nr_votos() + 1;
      deputados_federais[i].set_nr_votos(votos);
    }
  }
}

bool DeputadoFederal::mostrar_candidato(int nr_candidato, DeputadoFederal deputados_federais[]){
  condicao_voto_nulo = true;
  cout << "VOTO PARA DEPUTADO FEDERAL" << endl;
  bool voto_na_legenda = false;
  string partido;

  if (nr_candidato == 0){
    cout << "VOTO EM BRANCO" << endl << endl;
    return false;
  }
  
  for(int i=0; i < NR_DEPUTADOS_FEDERAIS; i++){
    int nr_candidato_array = deputados_federais[i].get_nr_candidato();
    int nr_partido_array = deputados_federais[i].get_nr_partido();
    int cd_genero_array = deputados_federais[i].get_cd_genero();

    if(nr_candidato_array == nr_candidato){
      cout << "Nome: " << deputados_federais[i].get_nm_urna_candidato() << endl;
      cout << "Nome completo: " << deputados_federais[i].get_nm_candidato() << endl;
      cout << "Partido: " << deputados_federais[i].get_sg_partido() << endl;
      if (cd_genero_array == 2){
        cout << "Número do candidato: " << deputados_federais[i].get_nr_candidato() << endl << endl;
      }
      else {
        cout << "Número da candidata: " << deputados_federais[i].get_nr_candidato() << endl << endl;
      }
      condicao_voto_nulo = false;
    }

    if(nr_partido_array == nr_candidato){
      condicao_voto_nulo = false;
      voto_na_legenda = true;
      partido = deputados_federais[i].get_ds_partido();
    }
  }

  if(voto_na_legenda)
    cout << "VOTO NA LEGENDA DO PARTIDO: " << partido << endl << endl;

  if(condicao_voto_nulo)
    cout << "VOTO NULO" << endl << endl;

  return condicao_voto_nulo;
}
