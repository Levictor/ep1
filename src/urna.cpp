#include "urna.hpp"

vector<Eleitor> Urna::iniciar_votacao(Presidente presidentes[],
                            Governador governadores[],
                            Senador senadores[], 
                            DeputadoDistrital deputados_distritais[],
                            DeputadoFederal deputados_federais[],
                            vector<Eleitor> eleitor,
                            int numero_eleitores){

  for(int i=0; i<numero_eleitores; i++){
    system ("clear");
    cout << "Digite seu nome por favor" << endl;
    cin >> nome_eleitor;
    eleitor[i].set_nome_eleitor(nome_eleitor);
    cout << eleitor[0].get_nome_eleitor() << endl;

    //VOTAÇÃO PARA DEPUTADO FEDERAL
    condicao_voto = true;
    while(condicao_voto){
      system("clear");
      cout << "VOTAR PARA DEPUTADO FEDERAL" << endl;
      cout << "DIGITE O NUMERO DO SEU CANDIDATO"  << endl;
      cout << "CASO DESEJAR VOTAR EM BRANCO, DIGITE 'BRANCO' OU '0' PARA VOTAR EM BRANCO"  << endl;

      nr_voto = verificar_voto();


      system("clear");
      condicao_voto_nulo = deputados_federais->mostrar_candidato(nr_voto, deputados_federais);

      if(condicao_voto_nulo){
        if (this->confirmar_voto()){
          eleitor[i].set_voto_deputado_federal("NULO");
          votos_nulos++;
          condicao_voto = false;
        }
      }
      else{
        if (this->confirmar_voto()){
          if(nr_voto == 0)
            votos_brancos++;

          deputados_federais->votar(nr_voto,deputados_federais);
          eleitor[i].set_voto_deputado_federal(to_string(nr_voto));
          cout << "Numero" << eleitor[i].get_voto_deputado_federal() << endl;
          condicao_voto = false;
        }
      }
    }

    //VOTAÇÃO PARA DEPUTADO DISTRITAL
    condicao_voto = true;
    while(condicao_voto){
      system("clear");
      cout << "VOTAR PARA DEPUTADO DISTRITAL" << endl;
      cout << "DIGITE O NUMERO DO SEU CANDIDATO"  << endl;
      cout << "CASO DESEJAR VOTAR EM BRANCO, DIGITE 'BRANCO' OU '0' PARA VOTAR EM BRANCO"  << endl;

      nr_voto = verificar_voto();

      system("clear");
      condicao_voto_nulo = deputados_distritais->mostrar_candidato(nr_voto, deputados_distritais);

      if (condicao_voto_nulo){
        if (this->confirmar_voto()){
          eleitor[i].set_voto_deputado_distrital("NULO");
          votos_nulos++;
          condicao_voto = false;
        }
      }
      else{
        if (this->confirmar_voto()){
          if(nr_voto == 0)
            votos_brancos++;
          deputados_distritais->votar(nr_voto,deputados_distritais);
          eleitor[i].set_voto_deputado_distrital(to_string(nr_voto));
          condicao_voto = false;
        }
      }
    }

    //VOTAÇÃO PARA SENADOR PRIMEIRA VAGA
    condicao_voto = true;
    while(condicao_voto){
      system("clear");
      cout << "VOTAR PARA SENADOR PRIMEIRA VAGA" << endl;
      cout << "DIGITE O NUMERO DO SEU CANDIDATO"  << endl;
      cout << "CASO DESEJAR VOTAR EM BRANCO, DIGITE 'BRANCO' OU '0' PARA VOTAR EM BRANCO"  << endl;

      nr_voto = verificar_voto();


      system("clear");
      condicao_voto_nulo = senadores->mostrar_candidato(nr_voto, senadores, -2, 1);
      
      if (condicao_voto_nulo){
        if (this->confirmar_voto()){
          eleitor[i].set_voto_senador_1("NULO");
          votos_nulos++;
          condicao_voto = false;
        }
      }
      else{
        if (this->confirmar_voto()){
          if(nr_voto == 0)
            votos_brancos++;
          senadores->votar(nr_voto,senadores);
          eleitor[i].set_voto_senador_1(to_string(nr_voto));
          condicao_voto = false;
        }
      }
    }

    //VOTAÇÃO PARA SENADOR SEGUNDA VAGA
    condicao_voto = true;
    while(condicao_voto){
      system("clear");
      cout << "VOTAR PARA SENADOR SEGUNDA VAGA" << endl;
      cout << "DIGITE O NUMERO DO SEU CANDIDATO"  << endl;
      cout << "CASO DESEJAR VOTAR EM BRANCO, DIGITE 'BRANCO' OU '0' PARA VOTAR EM BRANCO"  << endl;

      nr_voto = verificar_voto();

      system("clear");
      string primeiro_voto = eleitor[i].get_voto_senador_1();

      if(primeiro_voto.compare("BRANCO") != 0 && primeiro_voto.compare("NULO") != 0)
        condicao_voto_nulo = senadores->mostrar_candidato(nr_voto, senadores, stoi(primeiro_voto,nullptr,10),2);
      else
        condicao_voto = senadores->mostrar_candidato(nr_voto,senadores,-2, 2);

      if (condicao_voto_nulo){
        if (this->confirmar_voto()){
          eleitor[i].set_voto_senador_2("NULO");
          votos_nulos++;
          condicao_voto = false;
        }
      }
      else {
        if (this->confirmar_voto()){
          if(nr_voto == 0)
            votos_brancos++;
          senadores->votar(nr_voto,senadores);
          eleitor[i].set_voto_senador_2(to_string(nr_voto));
          condicao_voto = false;
        }
      }
    }

    //VOTAÇÃO PARA GOVERNADOR
    condicao_voto = true;
    while(condicao_voto){
      system("clear");
      cout << "VOTAR PARA GOVERNADOR" << endl;
      cout << "DIGITE O NUMERO DO SEU CANDIDATO"  << endl;
      cout << "CASO DESEJAR VOTAR EM BRANCO, DIGITE 'BRANCO' OU '0' PARA VOTAR EM BRANCO"  << endl;

      nr_voto = verificar_voto();


      system("clear");

      condicao_voto_nulo = governadores->mostrar_candidato(nr_voto, governadores);

      if (condicao_voto_nulo){
        if (this->confirmar_voto()){
          eleitor[i].set_voto_governador("NULO");
          votos_nulos++;
          condicao_voto = false;
        }
      }
      else{
        if (this->confirmar_voto()){
          if(nr_voto == 0)
            votos_brancos++;
          governadores->votar(nr_voto,governadores);
          eleitor[i].set_voto_governador(to_string(nr_voto));
          condicao_voto = false;
        }
      }
    }

    //VOTAÇÃO PARA PRESIDENTE
    condicao_voto = true;
    while(condicao_voto){
      system("clear");
      cout << "VOTAR PARA PRESIDENTE" << endl;
      cout << "DIGITE O NUMERO DO SEU CANDIDATO"  << endl;
      cout << "CASO DESEJAR VOTAR EM BRANCO, DIGITE 'BRANCO' OU '0'"  << endl;

      nr_voto = verificar_voto();

      system("clear");
      condicao_voto_nulo = presidentes->mostrar_candidato(nr_voto, presidentes);

      if(condicao_voto_nulo){
        if (this->confirmar_voto()){
          eleitor[i].set_voto_presidente("NULO");
          votos_nulos++;
          condicao_voto = false;
        }
      }
      else{
        if (this->confirmar_voto()){
          if(nr_voto == 0)
            votos_brancos++;
          presidentes->votar(nr_voto,presidentes);
          eleitor[i].set_voto_presidente(to_string(nr_voto));
          condicao_voto = false;
        }
      }
    }
  }
  return eleitor;
}

int Urna::verificar_voto(){

  while(true){
    cin >> voto;
    if (voto.compare("BRANCO") == 0){
      return 0;
    }

    else {
      try{
        return stoi(voto,nullptr,10); 
      }

      catch(exception &e){
        cout << "Por favor, certifique-se que digitou corretamente o que foi pedido" << endl;
      }
    }
  }
}

bool Urna::confirmar_voto(){
  cout << "Digite CONFIRMAR para confirmar seu voto" << endl;
  cout << "Digite CORRIGE para corrigir seu voto" << endl << endl;
  while(true){

    cin >> decisao_voto;

    if (decisao_voto.compare("CORRIGE") == 0){
      return false;
    }

    else if (decisao_voto.compare("CONFIRMAR") == 0){
      system("clear");
      return true;
    }

    else {
      cout << "Por favor, certifique-se que digitou corretamente o que foi pedido" << endl;
    }
  }
}

int Urna::get_votos_brancos(){
  return this->votos_brancos;
}

int Urna::get_votos_nulos(){
  return this->votos_nulos;
}
