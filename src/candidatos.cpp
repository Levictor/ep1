#include "candidatos.hpp"

void Candidatos::set_nm_ue(string nm_ue){
    this->NM_UE = nm_ue;
}
string Candidatos::get_nm_ue(){
    return this->NM_UE;
}

void Candidatos::set_ds_cargo(string ds_cargo){
    this->DS_CARGO = ds_cargo;
}
string Candidatos::get_ds_cargo(){
    return this->DS_CARGO;
}

void Candidatos::set_nr_candidato(string nr_candidato){
    this->NR_CANDIDATO = stoi(nr_candidato, nullptr, 10);
}
int Candidatos::get_nr_candidato(){
    return this->NR_CANDIDATO;
}

void Candidatos::set_nm_candidato(string nm_candidato){
    this->NM_CANDIDATO = nm_candidato;
}
string Candidatos::get_nm_candidato(){
    return this->NM_CANDIDATO;
}

void Candidatos::set_nm_urna_candidato(string nm_urna_candidato){
    this->NM_URNA_CANDIDATO = nm_urna_candidato;
}
string Candidatos::get_nm_urna_candidato(){
    return this->NM_URNA_CANDIDATO;
}

void Candidatos::set_nr_partido(string nr_partido){
    this->NR_PARTIDO = stoi(nr_partido, nullptr, 10);
}
int Candidatos::get_nr_partido(){
    return this->NR_PARTIDO;
}

void Candidatos::set_sg_partido(string sg_partido){
    this->SG_PARTIDO = sg_partido;
}
string Candidatos::get_sg_partido(){
    return this->SG_PARTIDO;
}

void Candidatos::set_ds_partido(string ds_partido){
    this->DS_PARTIDO = ds_partido;
}
string Candidatos::get_ds_partido(){
    return this->DS_PARTIDO;
}

void Candidatos::set_nr_idade_posse(string nr_idade_posse){
    this->NR_IDADE_POSSE = stoi(nr_idade_posse, nullptr, 10);
}
int Candidatos::get_nr_idade_posse(){
    return this->NR_IDADE_POSSE;
}

void Candidatos::set_cd_genero(string cd_genero){
    this->CD_GENERO = stoi(cd_genero, nullptr, 10);
}
int Candidatos::get_cd_genero(){
    return this->CD_GENERO;
}

void Candidatos::set_nr_votos(int nr_votos){
    this->numero_votos = nr_votos;
}
int Candidatos::get_nr_votos(){
    return this->numero_votos;
}

void Candidatos::mostrar_candidato(){
    
}
