#include "presidente.hpp"

Presidente::Presidente(){
  
}

void Presidente::preencher(Presidente presidentes[]){
  int i=0;

  fstream ip("./data/consulta_cand_2018_BR.csv");

  if(!ip.good())
    cout<<"Não foi possivel abrir o arquivo"<<endl;

  string nm_ue;
  string ds_cargo;
  string nr_candidato;
  string nm_candidato;
  string nm_urna_candidato;
  string nr_partido;
  string sg_partido;
  string ds_partido;
  string nr_idade_posse;
  string cd_genero;

  while(ip.good()){

    getline(ip,nm_ue,',');
    getline(ip,ds_cargo,',');
    getline(ip,nr_candidato,',');
    getline(ip,nm_candidato,',');
    getline(ip,nm_urna_candidato,',');
    getline(ip,nr_partido,',');
    getline(ip,sg_partido,',');
    getline(ip,ds_partido,',');
    getline(ip,nr_idade_posse,',');
    getline(ip,cd_genero);

    presidentes[i].set_nr_votos(0);
    presidentes[i].set_nm_ue(nm_ue);
    presidentes[i].set_ds_cargo(ds_cargo);
    presidentes[i].set_nr_candidato(nr_candidato);
    presidentes[i].set_nm_candidato(nm_candidato);
    presidentes[i].set_nm_urna_candidato(nm_urna_candidato);
    presidentes[i].set_nr_partido(nr_partido);
    presidentes[i].set_sg_partido(sg_partido);
    presidentes[i].set_ds_partido(ds_partido);
    presidentes[i].set_nr_idade_posse(nr_idade_posse);
    presidentes[i].set_cd_genero(cd_genero);
    
    i++;
    
  }
}

void Presidente::votar(int nr_candidato, Presidente presidentes[]){
  for(int i=0; i < NR_PRESIDENTES; i++){
    int nr_candidato_array = presidentes[i].get_nr_candidato();

    if(nr_candidato_array == nr_candidato){
      int votos = presidentes[i].get_nr_votos() + 1;
      presidentes[i].set_nr_votos(votos);
    }
  }
}

bool Presidente::mostrar_candidato(int nr_candidato, Presidente presidentes[]){
  condicao_voto_nulo = true;
  cout << "VOTO PARA PRESIDENTE" << endl;
  
  if (nr_candidato == 0){
    cout << "VOTO EM BRANCO" << endl << endl;
    return false;
  }
  for(int i=0; i < NR_PRESIDENTES; i++){
    int nr_candidato_array = presidentes[i].get_nr_candidato();
    int cd_genero_array = presidentes[i].get_cd_genero();
    string cargo_candidato_array = presidentes[i].get_ds_cargo();
    

    if(nr_candidato_array == nr_candidato && cargo_candidato_array.compare("PRESIDENTE") == 0){
      cout << "Nome: " << presidentes[i].get_nm_urna_candidato() << endl;
      cout << "Nome completo: " << presidentes[i].get_nm_candidato() << endl;
      cout << "Partido: " << presidentes[i].get_sg_partido() << endl;
      if (cd_genero_array == 2){
        cout << "Número do candidato: " << presidentes[i].get_nr_candidato() << endl;
      }
      else {
        cout << "Número da candidata: " << presidentes[i].get_nr_candidato() << endl;
      }
      condicao_voto_nulo = false;
    }
    if (cargo_candidato_array.compare("VICE-PRESIDENTE") == 0 && nr_candidato_array == nr_candidato)
      vice_presidente = presidentes[i].get_nm_candidato();
  }

  if(condicao_voto_nulo){
    cout << "VOTO NULO" << endl << endl;
  }

  if(!condicao_voto_nulo)
    cout << "VICE PRESIDENTE: " << vice_presidente << endl << endl;

  return condicao_voto_nulo;
}
