#include "eleitor.hpp"

Eleitor::Eleitor(){
    this->set_nome_eleitor("");
    this->set_voto_deputado_distrital("");
    this->set_voto_deputado_federal("");
    this->set_voto_governador("");
    this->set_voto_presidente("");
    this->set_voto_senador_1("");
    this->set_voto_senador_2("");
}
Eleitor::~Eleitor(){}

void Eleitor::set_voto_deputado_federal(string voto_deputado_federal){
    this->voto_deputado_federal = voto_deputado_federal;
}

string Eleitor::get_voto_deputado_federal(){
    return this->voto_deputado_federal;
}

void Eleitor::set_voto_deputado_distrital(string voto_deputado_distrital){
    this->voto_deputado_distrital = voto_deputado_distrital;
}

string Eleitor::get_voto_deputado_distrital(){
    return this->voto_deputado_distrital;
}

void Eleitor::set_voto_senador_1(string voto_senador_1){
    this->voto_senador_1 = voto_senador_1;
}

string Eleitor::get_voto_senador_1(){
    return this->voto_senador_1;
}

void Eleitor::set_voto_senador_2(string voto_senador_2){
    this->voto_senador_2 = voto_senador_2;
}

string Eleitor::get_voto_senador_2(){
    return this->voto_senador_2;
}

void Eleitor::set_voto_governador(string voto_governador){
    this->voto_governador = voto_governador;
}

string Eleitor::get_voto_governador(){
    return this->voto_governador;
}

void Eleitor::set_voto_presidente(string voto_presidente){
    this->voto_presidente = voto_presidente;
}

string Eleitor::get_voto_presidente(){
    return this->voto_presidente;
}

void Eleitor::set_nome_eleitor(string nome_eleitor){
    this->nome_eleitor = nome_eleitor;
}

string Eleitor::get_nome_eleitor(){
    return this->nome_eleitor;
}

void Eleitor::relatar_votos(Presidente presidentes[], Governador governadores[], Senador senadores[], DeputadoDistrital deputadosDistritais[], DeputadoFederal deputadosFederais[], vector<Eleitor> eleitores, int numero_eleitor){
    system("clear");
    for(int i=0;i<numero_eleitor;i++){
        cout << "NOME DO ELEITOR: " << eleitores[i].get_nome_eleitor() << endl << endl;

        nr_voto_eleitor = tratar_voto(eleitores[i].get_voto_deputado_federal());
        deputadosFederais->mostrar_candidato(nr_voto_eleitor, deputadosFederais);

        nr_voto_eleitor = tratar_voto(eleitores[i].get_voto_deputado_distrital());
        deputadosDistritais->mostrar_candidato(nr_voto_eleitor, deputadosDistritais);

        nr_voto_eleitor = tratar_voto(eleitores[i].get_voto_senador_2());
        senadores->mostrar_candidato(nr_voto_eleitor, senadores,-1,2);

        nr_voto_eleitor = tratar_voto(eleitores[i].get_voto_senador_1());
        senadores->mostrar_candidato(nr_voto_eleitor, senadores,-1,1);

        nr_voto_eleitor = tratar_voto(eleitores[i].get_voto_governador());
        governadores->mostrar_candidato(nr_voto_eleitor, governadores);

        nr_voto_eleitor = tratar_voto(eleitores[i].get_voto_presidente());
        presidentes->mostrar_candidato(nr_voto_eleitor, presidentes);
    
        cout << "----------------------------------------" << endl;
    }
}

int Eleitor::tratar_voto(string voto){
    cout << voto << endl;
    if (voto.compare("NULO") == 0){
        return -2;
    }
    else if(voto.compare("BRANCO") == 0){
        return 0;
    }
    else{
        return stoi(voto,nullptr,10);
    }
}
