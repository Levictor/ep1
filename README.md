# Exercício de Programação 1

Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps_2018_2/ep1/wikis/Descricao)

## Como usar o projeto

Primeiramente, execute o comando:

`git clone https://gitlab.com/Levictor/ep1 && cd ep1`

Em seguida, para compilar o código, execude o comando:

`make`

Logo após, para executar o programa, execute o comando:

`make run`

Agora, com o programa executado, digite a quantidade de eleitores que irão votar e pressione enter.

Após o passo anterior a votação irá começar, com a quantidade de eleitores que foi digitado anteriormente, os eleitores agora devem forecer seus nomes e votar de acordo com o candidato que desejar.

Para votar no seu candidato, o eleitor deve fornecer o número do candidato que deseja votar, caso desejar votar em branco o eleitor deverá digitar *BRANCO* ou *0*, todo e qualquer número que for digitado que nao corresponder ao número do candidato que está concorrendo ao cargo que está escrito acima, será contado como voto nulo.

Para os votos dos deputados, o eleitor poderá fazer o voto na legenda o qual o mesmo colocará o número do partido o qual deseja votar e seu voto irá para o deputado com mais votos do partido que o eleitor digitar

Tendo escolhido seu candidato, o eleitor irá se deparar com as informações sobre o seu candidato, os vice-presidentes, vice-governadores, primeiros suplentes e segundos suplentes terão somente seus nomes de acordo com registrado nas eleiçoes a mostra, e deverá digitar *CONFIRMAR* ou *CANCELA* a opção *CONFIRMAR* irá confirmar o voto e o mesmo será computado pela urna, caso o eleitor digitar *CANCELA* o voto será cancelado e o eleitor irá votar novamente para o cargo que estava votando anteriormente, e o voto anterior não será computado.


## Funcionalidades do projeto

Após todos os eleitores votarem, o programa irá mostrar em quais candidatos cada eleitor votou e quais foram os vencedores dos cargos de senador, governador e presidente, caso o presidente com maior quantidade de votos nao atingir 50% dos votos eleirá para o segundo turno com o presidente com a segunda maior quantidade de votos, o mesmo ocorrerá para o cargo de governador, todavia, para o cargo de senador, tendo em vista que não possui segundo turno, caso ocorra um empate pela segunda vaga, o candidato mais velho tomará posse do cargo, o mesmo ocorrerá se houver o segundo turno dos cargos de presidente e de governador.

## Bugs e problemas
O programa não funciona bem se for digitado um número superior a 1000000, então, por favor, evite.

Não é mostrado os deputados vencedores, devido a falta deconhecimento sobre a política sobre os mesmos.

## Referências
http://www.cplusplus.com;

https://stackoverflow.com;

https://gitlab.com/oofga.